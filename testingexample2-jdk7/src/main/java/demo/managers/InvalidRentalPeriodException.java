package demo.managers;

public class InvalidRentalPeriodException extends Exception {
	private static final long serialVersionUID = -4622372412538517953L;
	public InvalidRentalPeriodException(String msg) {
		super(msg);
	}
}
