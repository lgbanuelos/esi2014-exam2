package demo.managers;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import org.joda.time.LocalDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import demo.Application;
import demo.models.Plant;
import demo.models.PlantRepository;
import demo.models.PurchaseOrder;
import demo.models.PurchaseOrderRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
public class PurchaseOrderManagerTests {
	@Autowired
	PlantRepository plantRepo;
	@Autowired
	PurchaseOrderRepository poRepo;
	@Autowired
	PurchaseOrderManager poManager;
	
	@Test
	public void testCreatePO() throws Exception {
		PurchaseOrder po = new PurchaseOrder();
		poManager.createPO(po);
	}	
}
